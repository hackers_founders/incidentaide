# README #

---

## IncidentAid ##

This repository of the Django-powered website for IncidentAid (formerly known as *Incident Aide*, hence the repo name)

### Installation ###

1. This repository uses [Git](https://git-scm.com/) for revision control. Begin by cloning this repository.
2. Make sure you have Python 2.7 installed. Python 3.4 might work, but this can't be guaranteed.
3. Install dependencies from [PyPI](https://pypi.python.org/pypi) using [pip](https://pip.pypa.io/en/stable/). This can be done from the the repository's root directory:
        
    ```
    pip install -r requirements/local.txt
    ```

    or from within the */requirements* directory:

    ```
    pip install -r local.txt
    ```

4. Apply migrations and create the development SQLite database:

    ```
    python manage.py migrate
    ```

5. Run the Django development server:

    ```
    python manage.py runserver
    ```

### Other Recommendations ###

[Sublime Text](http://www.sublimetext.com/) is highly recommended as a code editor. To get the most out of working with Django, it's also recommended to add [Package Control](https://packagecontrol.io/) to Sublime and use it to install [Djaneiro](https://github.com/squ1b3r/Djaneiro) (Django-specific syntax highlighting and code-completion snippets).

### Authors and Contributors ###

This repository was started and is maintained by [Christopher Betts](https://bitbucket.org/kaikaichen/).