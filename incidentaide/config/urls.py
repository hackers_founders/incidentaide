# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.views.defaults import bad_request, permission_denied, page_not_found, server_error

from pages import views

urlpatterns = [
    url(
        r'^#(?P<anchor>[-_\w]+)$',
        view=views.Homepage.as_view(),
        name="home"
    ),
    url(
        r'^$',
        view=views.Homepage.as_view(),
        name="home"
    ),
    url(r'^admin/',
        include([
            url(r'^doc/',
                include('django.contrib.admindocs.urls')
            ),
            url(r'^',
                admin.site.urls
            ),
        ])
    ),
    url(r'^captcha/',
        include('captcha.urls')
    ),
    url(r'^markdown/',
        include('django_markdown.urls')
    ),
]

if 'blog' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^blog/',
            include('blog.urls')
        ),
    ]

# Serve media files in development
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [

        url(r'^__debug__/',
            include(debug_toolbar.urls)
        ),

        # This allows the error pages to be debugged during development, just visit
        # these URLs in browser to see how these error pages look.
        url(
            r'^400/$',
            view=bad_request,
            name='400_demo'
        ),
        url(
            r'^403/$',
            view=permission_denied,
            name='403_demo'
        ),
        url(
            r'^404/$',
            view=page_not_found,
            name='404_demo'
        ),
        url(
            r'^500/$',
            view=server_error,
            name='500_demo'
        ),

    ]

admin.site_title = _('IncidentAid Site Admin')
admin.site_header = _('IncidentAid Administration')