# -*- coding: utf-8 -*-
"""Development settings and globals."""

from __future__ import absolute_import, unicode_literals
from os.path import join, normpath
from .common import *

DEBUG = True

SECRET_KEY = '9#p&#gd*4eem5h__obioq0pgg-rf#be&@(o*ig(=&2tbeo2n1+'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': normpath(join(CONFIG_DIR, 'development.sqlite3')),
    }
}

INSTALLED_APPS += (
    'django.contrib.admindocs',
    
    'debug_toolbar',
)

STATIC_ROOT = normpath(join(ROOT_DIR, 'assets', 'static'))
MEDIA_ROOT = normpath(join(ROOT_DIR, 'assets', 'uploads'))

META_SITE_DOMAIN = 'localhost:8000'


# Email Settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST_USER = 'contact@incidentaid.com'