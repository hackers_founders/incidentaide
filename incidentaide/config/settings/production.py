# -*- coding: utf-8 -*-
"""Production settings and globals."""

from __future__ import absolute_import, unicode_literals
from os.path import join, dirname, normpath
import json

from django.core.exceptions import ImproperlyConfigured

from .common import *


DEBUG = False

with open(normpath(join(CONFIG_DIR, 'settings', 'secrets.json'))) as secrets_file:
    secrets = json.loads(secrets_file.read())

def get_secret(setting, secrets=secrets):
    """Get the secrets variable or return explicit exception."""
    try:
        return secrets[setting]
    except KeyError:
        error_msg = "Set the {0} value".format(setting)
        raise ImproperlyConfigured(error_msg)


SECRET_KEY = get_secret('DJANGO_SECRET_KEY')
ALLOWED_HOSTS = tuple(get_secret('DJANGO_ALLOWED_HOSTS'))

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = get_secret('DJANGO_EMAIL_HOST')
EMAIL_HOST_PASSWORD = get_secret('DJANGO_EMAIL_HOST_PASSWORD')
EMAIL_HOST_USER = get_secret('DJANGO_EMAIL_HOST_USER')
EMAIL_PORT = get_secret('DJANGO_EMAIL_PORT')
EMAIL_USE_TLS = True
SERVER_EMAIL = get_secret('DJANGO_SERVER_EMAIL')

ADMINS = tuple(tuple(admin) for admin in get_secret('DJANGO_ADMINS'))

DATABASES = {
    'default': {
        'ENGINE': "django.db.backends.postgresql_psycopg2",     # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': "incidentaide_db",                              # Or path to database file if using sqlite3.
        'USER': get_secret('DJANGO_DB_USERNAME'),               # Not used with sqlite3.
        'PASSWORD': get_secret('DJANGO_DB_PASSWORD'),           # Not used with sqlite3.
        'HOST': get_secret('DJANGO_DB_HOST'),                   # Set to empty string for localhost. Not used with sqlite3.
        'PORT': get_secret('DJANGO_DB_PORT'),                   # Set to empty string for default. Not used with sqlite3.
    }
}

STATIC_ROOT = normpath(join(dirname(ROOT_DIR), 'assets', 'static'))
MEDIA_ROOT = normpath(join(dirname(ROOT_DIR), 'assets', 'uploads'))

META_SITE_DOMAIN = 'incidentaid.com'