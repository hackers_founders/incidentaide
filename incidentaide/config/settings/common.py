# -*- coding: utf-8 -*-
"""Common settings and globals for this project."""

from __future__ import absolute_import, unicode_literals
from os.path import abspath, basename, dirname, join, normpath

from django.contrib.messages import constants as messages


SETTINGS_DIR = normpath(dirname(abspath(__file__)))
CONFIG_DIR = normpath(dirname(SETTINGS_DIR))
PROJECT_DIR = normpath(dirname(CONFIG_DIR))
ROOT_DIR = normpath(dirname(PROJECT_DIR))
PROJECT_NAME = basename(PROJECT_DIR)


DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Optional Django apps:
    'django.contrib.humanize',
    'django.contrib.sites',
)

THIRD_PARTY_APPS = (
    'bootstrapform',
    'captcha',
    'django_extensions',
    'django_markdown',
    'django_social_share',
    'meta',
)

LOCAL_APPS = (
    'about',
    # 'blog',
    'pages',
    'utils',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MESSAGE_TAGS = {
    messages.DEBUG: 'well',
    messages.INFO: 'alert alert-info',
    messages.SUCCESS: 'alert alert-success',
    messages.WARNING: 'alert alert-warning',
    messages.ERROR: 'alert alert-danger'
}

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

MIGRATION_MODULES = {
    'sites': 'migration_modules.contrib.sites.migrations',
    'captcha': 'migration_modules.captcha.migrations',
}

ROOT_URLCONF = '{}.urls'.format(basename(CONFIG_DIR))

TEMPLATES = ({
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': (normpath(join(PROJECT_DIR, 'templates')),),
    'APP_DIRS': True,
    'OPTIONS': {
        'context_processors': (
            'django.template.context_processors.debug',
            'django.template.context_processors.media',
            'django.template.context_processors.static',
            'django.template.context_processors.request',
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
            'utils.context_processors.misc',
        ),
    },
},)

WSGI_APPLICATION = '{}.wsgi.application'.format(basename(CONFIG_DIR))

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'US/Pacific'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/assets/static/'
STATICFILES_DIRS = (normpath(join(PROJECT_DIR, 'static')),)
MEDIA_URL = '/assets/media/'

SITE_ID = 1

MARKDOWN_EXTENSIONS = ()
MARKDOWN_EDITOR_SKIN = 'simple'

META_SITE_PROTOCOL = 'http'
META_PROJECT_NAME = 'IncidentAid'
META_SITE_TYPE = 'website'
META_USE_SITES = False
META_USE_OG_PROPERTIES = True
META_USE_TWITTER_PROPERTIES = True

CAPTCHA_IMAGE_BEFORE_FIELD = False
CAPTCHA_FONT_SIZE = 32
CAPTCHA_LENGTH = 5

FAVICON_URL = STATIC_URL + 'images/favicon.ico'