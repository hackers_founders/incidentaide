# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import Bio

@admin.register(Bio)
class TeamBioAdmin(admin.ModelAdmin):
    fields = (
        ('display_order', 'user'),
        'image',
        'job_title',
        'bio_text',
        )
    list_display = (
        'get_user_full_name',
        'display_order',
        'job_title',
        'bio_text',
        )
    list_editable = (
        'display_order',
        )
    list_select_related = ('user',)
    save_on_top = True

    def get_user_full_name(self, obj):
        return obj.user.get_full_name()
    get_user_full_name.short_description = _("name")