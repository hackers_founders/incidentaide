# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.apps import AppConfig

class AboutConfig(AppConfig):
    name = "about"
    verbose_name = "IncidentAid Info"