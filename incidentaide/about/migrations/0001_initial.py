# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Bio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(verbose_name='name slug')),
                ('display_order', models.PositiveSmallIntegerField(help_text='Number representing the spot in the order in which the bio will be displayed.', unique=True, verbose_name='display order')),
                ('image', models.ImageField(upload_to='images/team', verbose_name='picture')),
                ('job_title', models.CharField(max_length=20, verbose_name='job title/position')),
                ('bio_text', models.TextField(help_text="The team member's bio goes here.", verbose_name='bio text', blank=True)),
                ('user', models.OneToOneField(verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('display_order',),
                'verbose_name': 'team bio',
                'verbose_name_plural': 'team bios',
            },
        ),
    ]
