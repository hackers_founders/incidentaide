# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class Bio(models.Model):
    """
    Stores a single team member bio. Has one-to-one relationship with
    the user model defined by ``settings.AUTH_USER_MODEL``
    (:model:`auth.User` by default)

    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_("user"))
    slug = models.SlugField(_("name slug"))
    display_order = models.PositiveSmallIntegerField(_("display order"),
        unique=True,
        help_text=_("Number representing the spot in the order in which the bio will be "
                    "displayed.")
    )
    image = models.ImageField(_("picture"), upload_to="images/team")
    job_title = models.CharField(_('job title/position'), max_length=40)
    bio_text = models.TextField(_("bio text"),
        blank=True,
        help_text=_("The team member's bio goes here.")
    )

    class Meta:
        verbose_name = _("team bio")
        verbose_name_plural = _("team bios")
        ordering = ('display_order',)

    def __str__(self):
        return self.slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.user.get_full_name())
        super(Bio, self).save(*args, **kwargs)