# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from os.path import abspath, basename, dirname

from django.apps import AppConfig

APP_NAME = basename(dirname(abspath(__file__)))

class CustomAppConfig(AppConfig):
    name = APP_NAME
    verbose_name = APP_NAME