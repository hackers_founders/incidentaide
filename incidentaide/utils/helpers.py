from __future__ import unicode_literals


def anchorify(string):
    """
    Provides a quick and dirty method of converting `%23` back into `#`
    within a string, making it suitable for use in URLs containing an
    anchor.
    """
    return string.replace('%23', '#')