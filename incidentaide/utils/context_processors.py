# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from collections import OrderedDict

from django.conf import settings
from django.core.urlresolvers import reverse_lazy

from utils.helpers import anchorify

def misc(request):

    nav_links = OrderedDict([
        ('features', anchorify(reverse_lazy('home', kwargs={'anchor': 'features'}))),
        ('contact', anchorify(reverse_lazy('home', kwargs={'anchor': 'contact'}))),
    ])

    if 'blog' in settings.INSTALLED_APPS:
        nav_links.update({
            ('blog', reverse_lazy('blog:index')),
        })

    context = {
        'favicon_url': settings.FAVICON_URL,
        'nav_links': nav_links,
    }

    return context