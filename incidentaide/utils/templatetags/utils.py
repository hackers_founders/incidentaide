from __future__ import absolute_import, unicode_literals

from django import template
from django.template.defaultfilters import stringfilter

from ..helpers import anchorify as _anchorify


register = template.Library()

@register.filter(is_safe=True)
@stringfilter
def anchorify(string):
    return _anchorify(string)