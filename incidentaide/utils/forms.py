# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _


class ContactForm(forms.Form):
    """
    Form for sending an email to the website owner.

    """
    sender_name = forms.CharField(
        label=_('Name'),
        widget=forms.TextInput(
            attrs={
                'class': "form-control",
                'aria-describedby': "id_sender_name_status",
                'placeholder': _("Your name"),
                }
            )
        )
    sender_email = forms.EmailField(
        label=_('Email'),
        widget=forms.EmailInput(
            attrs={
                'class': "form-control",
                'aria-describedby': "id_sender_email_status",
                'placeholder': _("Your email address"),
                }
            )
        )
    subject = forms.CharField(
        label=_('Subject'),
        widget=forms.TextInput(
            attrs={
                'class': "form-control",
                'aria-describedby': "id_subject_status",
                'placeholder': _("Email subject"),
                }
            )
        )
    message = forms.CharField(
        label=_('Message'),
        widget=forms.Textarea(
            attrs={
                'class': "form-control",
                'rows': "11",
                'placeholder': _("Your message"),
                }
            )
        )
    antispam = forms.CharField(
        label=_('Leave this empty'),
        required=False,
        widget=forms.TextInput(
            attrs={
                'class': "form-control hidden",
                'placeholder': _("Leave this empty"),
                'type': 'email'
                }
            )
        )


    def clean_antispam(self):
        """
        Checks if the `antispam` field contains any data. If it does, a 
        ValidationError is raised.
        """
        if self.cleaned_data['antispam']:
            raise forms.ValidationError(_("This field is supposed to be left blank. A spambot may"
                                        " have filled it out."))