/* Performs a smooth page scroll to an anchor on the same page. */

$(document).ready(function() {
    $('a[data-smoothscroll]').click(function(event) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    $('[data-scrolltop]').click(function(event){
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
});
