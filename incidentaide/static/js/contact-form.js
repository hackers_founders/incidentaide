function renderFormWithErrors(errorDict) {

  var formGroup;
  var formGroupSelector;
  var fieldName;
  var fieldNameSelector;

  // Clear previous error messages upon form reload
  $('.error-msg').remove();
  $('.form-group').removeClass('has-error has-feedback');

  // Upon form reload, mark fields containing error-free data with .has-success
  $('.form-group:not(.has-error[type="hidden"]) .form-control').each(function() {
    formGroup = 'div_' + $(this).attr('id');
    formGroupSelector = '#' + formGroup;
    fieldName = $(this).attr('id');
    fieldNameSelector = '#' + fieldName;


    if ($(this).val() != '') {
        $(formGroupSelector).addClass('has-success has-feedback');
        var successFeedbackIcon = '<span class="form-control-feedback" aria-hidden="true"><span class="fa fa-check"></span></span>';
        var successFeedbackIconLabel = '<span id="' + fieldName + '_status" class="feedback-icon-label sr-only">(success)</span>';
        $(fieldNameSelector + '_status').remove();
        $(successFeedbackIconLabel).insertAfter(this);
        $(formGroupSelector + ' .form-control-feedback').remove();
        $(successFeedbackIcon).insertAfter(this);
    };


  });

  // Mark form fields containing errors with appropriate messages & feedback
  $.each(errorDict, function(field, Msg) {
    formGroup = 'div_id_' + field;
    formGroupSelector = '#' + formGroup;
    fieldName = 'id_' + field;

    fieldNameSelector = '#' + fieldName;
    
    $(formGroupSelector).addClass('has-error has-feedback').removeClass('has-warning');
    $(formGroupSelector + ' .warning-msg').remove();

    $('#error_' + field).remove();
    $('<span id="error_' + field + '" class="help-block error-msg text-danger">'+ Msg +'</span>').insertAfter(fieldNameSelector);
    
    var errorFeedbackIcon = '<span class="form-control-feedback" aria-hidden="true"><span class="fa fa-remove"></span></span>';
    var errorFeedbackIconLabel = '<span id="'+ fieldName + '_status" class="feedback-icon-label sr-only">(error)</span>';
    $(fieldNameSelector + '_status').remove();
    $(errorFeedbackIconLabel).insertAfter(fieldNameSelector);
    $(formGroupSelector + ' .form-control-feedback').remove();
    $(errorFeedbackIcon).insertAfter(fieldNameSelector);

  });
}

// Ajax submission logic
function sendEmail(event) {
  event.preventDefault();
  var contactForm = $(event.target);
  $.ajax({
    url: contactForm.attr('action'),
    type: contactForm.attr('method'),
    data: contactForm.serialize(),
    success: function(response) {
      $('#confirmation-message').html('<div class="alert alert-success"><p>' + response['success_message'] + '</p></div>')
      $('#contact-form-confirm').modal('toggle', 'handleupdate');

      $('#contact-form').trigger('reset');
      $('.form-group').removeClass('has-error has-success has-warning');
      $('.form-control-feedback').remove();
      $('.error-msg').remove();
      $('.warning-msg').remove();
    },
    error: function(response) {
      var responseDict = $.parseJSON(response.responseText);
      renderFormWithErrors(responseDict['form_errors']);
    }
  });
  
}

// Submit form via Ajax
$('#contact-form').submit(function(event) {
  sendEmail(event);
});
