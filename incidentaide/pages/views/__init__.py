# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from .home import Homepage

__all__ = [
    'Homepage'
]