# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from os.path import normpath, join

from django.conf import settings
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.core.mail import EmailMessage
from django.http import JsonResponse
from django.shortcuts import render, render_to_response, redirect
from django.utils.translation import ugettext as _
from django.views.generic import FormView

from meta.views import MetadataMixin

from about.models import Bio
from utils.forms import ContactForm
from .. import apps


class Homepage(MetadataMixin, SuccessMessageMixin, FormView):
    """
    Renders the homepage of the website, along with a ContactForm
    instance.

    **Context**

    ``form``
        An instance of *utils.ContactForm*.

    ``bio_list``
        A list of all :model:`about.Bio` objects.

    **Template:**

    :template:`pages/homepage.html`
    
    """
    template_name = normpath(join(apps.APP_NAME, 'homepage.html'))
    form_class = ContactForm
    success_url = reverse_lazy('home')
    success_message = _("Thank you for contacting IncidentAid! We’ll be in "
                        "touch as soon as possible.")

    # Metadata
    title = "IncidentAid"
    description = _("IncidentAid provides a clear, efficient smartphone "
                    "communication solution for front-line firefighters "
                    "and EMTs.")
    url = reverse_lazy('home')


    def get_context_data(self, **kwargs):
        context = super(Homepage, self).get_context_data(**kwargs)        
        context['bio_list'] = Bio.objects.select_related('user')
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            if form['antispam'].errors:
                return JsonResponse({'success_message': self.success_message})
            else:
                response_dict = {'form_errors': form.errors}
                return JsonResponse(response_dict, status=400)
        else:
            if form['antispam'].errors:
                messages.success(self.request, self.success_message)
                return redirect(self.get_success_url())
            else:
                return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        sender_name = form.cleaned_data.get('sender_name')
        sender_email = form.cleaned_data.get('sender_email')
        message_header = (
            "Sent by {} <{}> via {}:\n\n\n".format(sender_name, sender_email,
                                                   settings.EMAIL_HOST_USER)
        )
        body_content = message_header + form.cleaned_data.get('message')
        
        message = EmailMessage(
                      from_email=settings.EMAIL_HOST_USER,
                      body=body_content,
                      subject=form.cleaned_data.get('subject'),
                      to=[settings.EMAIL_HOST_USER]
                  )

        message.fail_silently = False if settings.DEBUG else True

        message.send()

        if self.request.is_ajax():
            response_dict = {
                'success_message': self.success_message,
                }
            return JsonResponse(response_dict)
        else:
            return super(Homepage, self).form_valid(form)
