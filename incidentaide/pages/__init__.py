# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from . import apps

default_app_config = '{}.apps.CustomAppConfig'.format(apps.APP_NAME)