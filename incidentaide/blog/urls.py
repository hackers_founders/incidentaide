# -*- coding: utf-8 -*-
from __future__ import absolute_import

from django.conf.urls import include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView

from . import views

app_name = 'blog'
urlpatterns = [
    url(
        r'^$',
        view=views.blog_index,
        name='index'
    ),
    url(
        r'^(?P<year>[0-9]{4})/',
        include([
            # Month by abbreviation, e.g. dec or sep
            url(
                r'^(?P<month>[-\w]+)/',
                view=views.blog_month_archive,
                name='month_archive'
            ),
        ])
    ),
    url(
        r'^(?P<slug>[-\w]+)/$',
        view=RedirectView.as_view(
            permanent=False,
            pattern_name="blog:post",
        )
    ),
    url(
        r'^feed/',
        include([
            url(
                r'^rss/$',
                view=views.BlogRSSFeed(),
                name='rss_feed'
            ),
            url(
                r'^atom/$',
                view=views.BlogAtomFeed(),
                name='atom_feed'
            ),
        ])
    ),
    url(r'^page/',
        include([
            url(
                r'^$',
                view=RedirectView.as_view(
                    permanent=False,
                    pattern_name="blog:index",
                    page=1
                )
            ),
            url(
                r'^(?P<page>[0-9]+)/$',
                view=views.blog_index,
                name='index'
            ),
        ])
    ),
    url(
        r'^post/(?P<slug>[-\w]+)/$',
        view=views.blog_post,
        name='post'
    ),
]