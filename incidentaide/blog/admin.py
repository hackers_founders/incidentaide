# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.template.defaultfilters import date as format_datetime
from django.utils.html import format_html
from django.utils.translation import ugettext as _

from django_markdown.admin import MarkdownModelAdmin

from .fields import AuthorChoiceField
from .models import STATUS_CHOICES, Post, Image


User = get_user_model()

class ImageInline(admin.TabularInline):
    """
    Allows for the adding of images from within `blog.PostAdmin`
    """
    model = Image


class PostAdminForm(forms.ModelForm):
    author = fields.AuthorChoiceField(queryset=None)

    def __init__(self, *args, **kwargs):
        super(PostAdminForm, self).__init__(*args, **kwargs)
        # set the widget with wrapper
        self.fields['author'].widget = admin.widgets.RelatedFieldWidgetWrapper(
            widget=forms.widgets.Select(),
            rel=Post._meta.get_field('author').rel,
            admin_site=self.admin_site,
            can_change_related=True,
            can_delete_related=False
        )
        self.fields['author'].queryset = User.objects.filter(is_staff=True)

    class Meta:
        model = Post
        fields = '__all__'

    class Media:
        ## media for the FilteredSelectMultiple widget
        css = {
            'all':('/media/css/widgets.css',),
        }
        # jsi18n is required by the widget
        js = ('/admin/jsi18n/',)


@admin.register(Post)
class PostAdmin(MarkdownModelAdmin):
    """
    Defines a ModelAdmin with Markdown capability for the `blog.Post` model.
    """
    actions = ('make_published', 'make_withdrawn',)
    date_hierarchy = 'pub_date'
    fieldsets = (
        ('Advanced options', {
            'classes': ('collapse',),
            'fields': (('created', 'modified'), 'pub_date', 'slug')
        }),
        ('Main options', {
            'fields':(
                ('status'),
                'author',
                'title',
                'content',
                'description',
                'primary_image',
            )
        }),
    )
    form = PostAdminForm
    inlines = (ImageInline,)
    list_display = ('title', 'author_display', 'monitored_pub_date', 'clickable_url', 'status')
    list_filter = ('author', 'status')
    prepopulated_fields = {
        'slug': ('title',)
    }
    readonly_fields = ('created', 'modified')
    search_fields = ('author__last_name', 'author__first_name', 'author__username',
                     'author__email', 'title', 'description')

    def __init__(self, model, admin_site):
        super(PostAdmin, self).__init__(model, admin_site)
        self.form.admin_site = admin_site # capture the admin_site

    def author_display(self, obj):
        """
        More detailed author information for ``ModelAdmin.list_display``.
        """
        if obj.author.get_full_name():
            display =  "{} [{}]".format(obj.author.get_username(), obj.author.get_full_name())
        else:
            display = obj.author.get_username()
        return display
    author_display.short_description = _("author")
    author_display.admin_order_field = "author__username"

    def clickable_url(self, obj):
        """Creates a direct clickable link to the post on the site."""
        link = format_html('<a href="{}">View on site&nbsp;&raquo;</a>', obj.get_absolute_url())
        return link
    clickable_url.allow_tags = True
    clickable_url.short_description = _("view on site")

    def make_published(self, request, queryset):
        """Marks selected posts in the admin as `published`."""
        rows_updated = queryset.update(status=STATUS_CHOICES.published)
        if rows_updated == 1:
            message_bit = "1 post was"
        else:
            message_bit == "{} posts were".format(rows_updated)
        self.message_user(request, "{} successfully marked as published.".format(message_bit))
    make_published.short_description = _("Mark selected posts as published")

    def make_withdrawn(self, request, queryset):
        """Marks selected posts in the admin as `withdrawn`."""
        rows_updated = queryset.update(status=STATUS_CHOICES.withdrawn)
        if rows_updated == 1:
            message_bit = "1 post was"
        else:
            message_bit == "{} posts were".format(rows_updated)
        self.message_user(request, "{} successfully marked as withdrawn.".format(message_bit))
    make_withdrawn.short_description = _("Mark selected posts as withdrawn")

    def monitored_pub_date(self, obj):
        """
        Displays `pub_date` only if the post's status is set to `published.`
        """
        if obj.status == obj.STATUS_CHOICES.published:
            return obj.pub_date
        elif obj.status == obj.STATUS_CHOICES.draft:
            return _("Not yet published")
        elif obj.status == obj.STATUS_CHOICES.withdrawn:
            return "[{}] {}".format(_("Withdrawn"), format_datetime(obj.pub_date, "N j, Y, P"))
    monitored_pub_date.short_description = _("Published")


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    """
    Admin site for adding/editing `blog.Image` instances.
    """
    date_hierarchy = 'created'
    fields = (
        ('created', 'modified'),
        'post',
        'description',
        'image_file',
        'external_url'
    )
    list_display = ('id', 'description', 'is_external')
    readonly_fields = ('created', 'modified')
