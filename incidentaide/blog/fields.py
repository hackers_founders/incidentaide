# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms


class AuthorChoiceField(forms.ModelChoiceField):
    """
    A subclass of ``forms.ModelChoiceField`` that displays the author's full
    name alongside the standard username, if possible.
    """

    def label_from_instance(self, obj):
        if obj.get_full_name():
            label = "{} [{}]".format(obj.get_username(), obj.get_full_name())
        else:
            label = obj.get_username()

        return label