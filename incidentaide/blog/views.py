# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from calendar import month_name
from datetime import datetime

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import NoReverseMatch, reverse_lazy
from django.template.defaultfilters import truncatewords
from django.utils.feedgenerator import Rss201rev2Feed, Atom1Feed
from django.utils.html import strip_tags
from django.views import generic

from markdown import markdown
from meta.views import MetadataMixin

from .models import Post, Image

class BlogMetadataMixin(MetadataMixin):
    object_type = 'article'
    title = "IncidentAid Blog"
    description = "The official blog of IncidentAid."


class BlogIndexView(BlogMetadataMixin, generic.ArchiveIndexView):
    """
    Displays the blog's home page (i.e. index).

    **Context**
    
    ``is_paginated``
        A boolean indicating whether the data is paginated.
        In this case, *True*.
    ``latest_posts``
        A queryset containing 5 published instances of :model:`blog.Post`.
    ``paginator``
        The Paginator object.
    ``page_obj``
        An instance of *Paginator.page*.
    ``date_list``
        A :model:`django.db.models.query.QuerySet.dates.DateQuerySet` object
        containing all years that have objects available, represented as 
        ``datetime.datetime objects``, in descending order.

    **Template:**

    :template:`blog/blog_post_list.html`
    
    """
    allow_empty = False
    allow_future = False
    context_object_name = 'post_list'
    date_field = "pub_date"
    date_list_period = 'month'
    paginate_by = 2 if settings.DEBUG else 5
    queryset = Post.objects.published().select_related('author', 'primary_image')
    template_name = 'blog/blog_post_list.html'

    def get_meta_url(self, context):
        try:
            meta_url = reverse_lazy('blog:index')
        except NoReverseMatch:
            meta_url = ""

        return meta_url


class BlogPostView(BlogMetadataMixin, generic.DetailView):
    """
    Displays an individual :model:`blog.Post` that has been published.

    **Context**

    ``post``
        An instance of :model:`blog.Post`.
    ``recent_posts``
        A queryset containing the 10 most recently-published instances of
        :model:`blog.Post`.

    **Template:**

    :template:`blog/blog_post.html`

    """
    allow_future = True
    queryset = Post.objects.published().select_related('author', 'primary_image')
    template_name = 'blog/blog_post.html'

    def get_context_data(self, **kwargs):
        context = super(BlogPostView, self).get_context_data(**kwargs)
        context.update({
            'recent_posts': self.queryset[0:10],
        })
        return context

    def get_meta_title(self, context):
        return "{} | {}".format(self.get_object().title, self.title)

    def get_meta_url(self, context):
        return self.get_object().get_absolute_url()

    def get_meta_image(self, context):
        img = self.get_object().primary_image
        if img is not None:
            return img.external_url if img.is_external() else image.get_absolute_url()
        else:
            return ""

    def get_meta_description(self, context):
        post = self.get_object()
        if post.description:
            return post.description
        else:
            # get the first 50 words of the post
            return truncatewords(strip_tags(markdown(post.content)), 50)
            



###### Uncomment the following block to enable the Year Archive View #######


# class BlogYearArchiveView(BlogMetadataMixin, generic.YearArchiveView):
#     """
#     Displays a yearly archive of published :model:`blog.Post` instances.

#     **Context**

#     ``post_list``
#         A queryset containing all published instances of :model:`blog.Post`
#         for the designated year.

#     **Template:**

#     :template:`blog/blog_year_archive.html`
    
#     """
#     date_field = "pub_date"
#     make_object_list = True
#     queryset = Post.objects.published()
#     template_name = "blog/blog_post_list.html"

#     def get_meta_url(self, context):
#         try:
#             meta_url = reverse_lazy(viewname="blog:year_archive",
#                                     kwargs={'year': self.get_year()})
#         except NoReverseMatch:
#             meta_url = ''

#         return meta_url

#     def get_meta_title(self, context):
#         return "Posts from {} | {}".format(self.get_year(), self.title)
# blog_year_archive = BlogYearArchiveView.as_view()
 

class BlogMonthArchiveView(BlogMetadataMixin, generic.MonthArchiveView):
    """
    Displays a monthly archive (within a given year) of published
    :model:`blog.Post` instances.

    **Context**

    ``post_list``
        A queryset containing all published instances of :model:`blog.Post`
        for the designated month of the year.
    ``date_list``
        A :model:`django.db.models.query.QuerySet.dates.DateQuerySet` object
        containing all years that have objects available, represented as 
        ``datetime.datetime objects``, in descending order.

    **Template:**

    :template:`blog/blog_month_archive.html`
    """
    date_field = "pub_date"
    date_list_period = 'month'
    month_format = '%b'
    queryset = Post.objects.published()
    template_name = "blog/blog_post_list.html"

    def get_meta_url(self, context):
        args = {'year': self.get_year(), 'month': self.get_month()}
        try:
            meta_url = reverse_lazy('blog:month_archive', kwargs=args)
        except NoReverseMatch:
            meta_url = ''

        return meta_url

    def get_meta_title(self, context):
        # convert month abbreviation to full name, e.g. sep --> September
        month_name = datetime.strptime(self.get_month(), '%b').strftime('%B')

        return "Posts from {} {} | {}".format(month_name, self.get_year(), 
                                              self.title)



class BlogRSSFeed(Feed):
    feed_copyright = "Copyright \u00A9 {} IncidentAid".format(datetime.today().year)
    description = "Updates on changes and new additions to the IncidentAid Blog"
    try:
        feed_url, link = reverse_lazy('blog:rss_feed'), reverse_lazy('blog:index')
    except NoReverseMatch:
        feed_url, link = '', ''

    def items(self):
        return Post.objects.published()[:10].select_related('author')
    
    def item_author_email(self, item):
        return item.author.email

    def item_author_name(self, item):
        return item.author.get_full_name()
        
    def item_description(self, item):
        return item.description

    def item_pubdate(self, item):
        return item.pub_date

    def item_title(self, item):
        return item.title

    def item_updateddate(self, item):
        return item.modified


class BlogAtomFeed(BlogRSSFeed):
    feed_type = Atom1Feed
    subtitle = BlogRSSFeed.description
    try:
        feed_url = reverse_lazy('blog:atom_feed')
    except NoReverseMatch:
        feed_url = ''


# Give FBV-like names to our class-based-views for shorter and more familiar-looking URLConfs
blog_index = BlogIndexView.as_view()
blog_post = BlogPostView.as_view()
blog_month_archive = BlogMonthArchiveView.as_view()