# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20150904_1713'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='pub_date',
            field=model_utils.fields.MonitorField(monitor='status', default=django.utils.timezone.now, when=set([1]), blank=True, help_text="Timestamp of when this blog post's status was set to `published`. Don't edit this unless absolutely necessary.", null=True, verbose_name='published'),
        ),
        migrations.AlterField(
            model_name='post',
            name='slug',
            field=models.SlugField(help_text='A short label, generally used in URLs.', unique=True, verbose_name='slug'),
        ),
        migrations.AlterField(
            model_name='post',
            name='title',
            field=models.CharField(unique=True, max_length=100, verbose_name='title'),
        ),
    ]
