# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import model_utils.fields
import django.db.models.deletion
import django_markdown.models
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('description', models.CharField(help_text='Short description of the image.', max_length=100, verbose_name='description')),
                ('image_file', models.ImageField(upload_to='images/blog/%Y/%m/%d', verbose_name='choose a file', blank=True)),
                ('external_url', models.URLField(help_text='Use if linking to an external image (use with caution).', verbose_name='external URL', blank=True)),
            ],
            options={
                'ordering': ['-created'],
                'get_latest_by': ['-created'],
                'verbose_name': 'image',
                'verbose_name_plural': 'images',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('status', models.PositiveIntegerField(default=0, verbose_name='status', choices=[(0, 'draft'), (1, 'published'), (2, 'withdrawn')])),
                ('pub_date', model_utils.fields.MonitorField(default=django.utils.timezone.now, when=set([1]), verbose_name='date published', monitor='status')),
                ('title', models.CharField(max_length=100, verbose_name='title')),
                ('slug', models.SlugField(help_text='A short label, generally used in URLs.', verbose_name='slug')),
                ('content', django_markdown.models.MarkdownField(help_text='Use Markdown syntax (preferable) or HTML tags.', verbose_name='content')),
                ('description', models.TextField(help_text='Metadata for SEO purposes.', verbose_name='description', blank=True)),
                ('author', models.ForeignKey(related_name='blog_posts', on_delete=django.db.models.deletion.SET_NULL, verbose_name='author', to=settings.AUTH_USER_MODEL, null=True)),
                ('primary_image', models.ForeignKey(related_name='+', blank=True, to='blog.Image', help_text='This will be the main image displayed.', null=True, verbose_name='primary image')),
            ],
            options={
                'ordering': ['-created'],
                'get_latest_by': 'pub_date',
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
            },
        ),
        migrations.AddField(
            model_name='image',
            name='post',
            field=models.ForeignKey(related_name='images', verbose_name='post', to='blog.Post'),
        ),
    ]
