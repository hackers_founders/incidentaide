# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20150904_1545'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='pub_date',
            field=model_utils.fields.MonitorField(default=django.utils.timezone.now, help_text="Timestamp of when this blog post's status was set to `published`. Don't edit this unless absolutely necessary.", when=set([1]), verbose_name='date published', monitor='status'),
        ),
        migrations.AlterField(
            model_name='post',
            name='status',
            field=models.PositiveIntegerField(help_text='The status of the blog post. Set a post to `withdrawn` instead of deleting it.', null=True, verbose_name='status', choices=[(0, 'Draft'), (1, 'Published'), (2, 'Withdrawn')]),
        ),
    ]
