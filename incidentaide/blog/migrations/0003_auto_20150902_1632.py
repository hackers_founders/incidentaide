# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_markdown.models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20150901_1924'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='post',
            options={'ordering': ['-created'], 'get_latest_by': 'pub_date', 'verbose_name': 'post', 'verbose_name_plural': 'posts'},
        ),
        migrations.AlterField(
            model_name='image',
            name='external_url',
            field=models.URLField(help_text='For linking to an external image (use with caution).', verbose_name='external URL', blank=True),
        ),
        migrations.AlterField(
            model_name='image',
            name='image_file',
            field=models.ImageField(help_text='**Does not support SVG or WebP formats.**', upload_to='images/blog/%Y/%m/%d', verbose_name='upload a file', blank=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='content',
            field=django_markdown.models.MarkdownField(help_text='The meat of the blog post. For document structure reasons, avoid using h1 headings.', verbose_name='content'),
        ),
        migrations.AlterField(
            model_name='post',
            name='primary_image',
            field=models.ForeignKey(related_name='+', blank=True, to='blog.Image', help_text='This will be the main image displayed. **Does not accept SVG or WebP formats**', null=True, verbose_name='primary image'),
        ),
        migrations.AlterField(
            model_name='post',
            name='status',
            field=models.PositiveIntegerField(default=0, help_text='The status of the blog post. Set a post to `withdrawn` instead of deleting it.', verbose_name='status', choices=[(0, 'Draft'), (1, 'Published'), (2, 'Withdrawn')]),
        ),
    ]
