# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='image_file',
            field=models.ImageField(upload_to='images/blog/%Y/%m/%d', verbose_name='upload a file', blank=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='status',
            field=models.PositiveIntegerField(default=0, help_text='Set a post to `withdrawn` instead of deleting it.', verbose_name='status', choices=[(0, 'draft'), (1, 'published'), (2, 'withdrawn')]),
        ),
    ]
