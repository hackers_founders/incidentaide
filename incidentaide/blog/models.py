# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.urlresolvers import NoReverseMatch, reverse
from django.db import models
from django.template.defaultfilters import date as format_datetime
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.utils.translation import ugettext as _

import pytz
from django_markdown.models import MarkdownField
from model_utils import Choices
from model_utils.fields import MonitorField
from model_utils.models import TimeStampedModel


STATUS_CHOICES = Choices(
    (0, 'draft', _('Draft')),
    (1, 'published', _('Published')),
    (2, 'withdrawn', _('Withdrawn'))
)

class PostQuerySet(models.query.QuerySet):
    def drafts(self):
        return self.filter(status=STATUS_CHOICES.draft).order_by('-created')
    
    def published(self):
        return self.filter(status=STATUS_CHOICES.published).order_by('-pub_date')

    def withdrawn(self):
        return self.filter(status=STATUS_CHOICES.withdrawn).order_by('-pub_date')


@python_2_unicode_compatible
class Post(TimeStampedModel):
    """
    Stores a single blog post, related to :model:`auth.User` and 
    :model:`blog.Image`.
    """
    status = models.PositiveIntegerField(_("status"),
        choices=STATUS_CHOICES,
        null=True,
        help_text=_("The status of the blog post. Set a post to `withdrawn` instead of "
                    "deleting it.")
    )
    pub_date = MonitorField(_("published"),
        monitor='status',
        when=[STATUS_CHOICES.published],
        help_text=_("Timestamp of when this blog post's status was set to `published`. Don't edit "
                    "this unless absolutely necessary."),
        null=True,
        blank=True
    )
    title = models.CharField(_("title"), max_length=100, unique=True)
    slug = models.SlugField(_("slug"),
        max_length=50,
        unique=True,
        help_text=_("A short label, generally used in URLs.")
    )
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
        verbose_name=_('author'),
        related_name="blog_posts",
        limit_choices_to={'is_staff': True},
        null=True,
        on_delete=models.SET_NULL
    )
    content = MarkdownField(_("content"),
        help_text=_("The meat of the blog post. For document structure reasons, avoid using h1 "
                    "headings."))
    description = models.TextField(_("description"),
        help_text=_("Synopsis of the article. Used for RSS feeds and meta tags.")
    )
    primary_image = models.ForeignKey('Image',
        verbose_name=_("primary image"),
        null=True,
        blank=True,
        related_name='+',
        help_text=_("This will be the main image displayed. **Does not accept SVG or WebP "
                    "formats**"))

    objects = models.Manager.from_queryset(PostQuerySet)()

    class Meta:
        ordering = ('-created',)
        verbose_name = _("post")
        verbose_name_plural = _("posts")
        get_latest_by = 'pub_date'

    def __str__(self):
        author_name = self.author.get_full_name()
        if not author_name:
            author_name = self.author.get_username()
        return '[{}] "{}"'.format(author_name, self.title)

    def get_absolute_url(self):
        """Returns the URL of the :model:`blog.Post` instance."""
        return reverse('blog:post', args=[self.slug])

    def clean(self):
        # create a slug if one doesn't exist
        if self.title and not self.slug:
            self.slug = slugify(self.title)
        if self.status == STATUS_CHOICES.draft:
            self.pub_date = None


@python_2_unicode_compatible
class Image(TimeStampedModel):
    """
    Stores information about an image for use with :model:`blog.Post`.
    """
    post = models.ForeignKey(Post, verbose_name=_("post"), related_name="images")
    description = models.CharField(_("description"),
        max_length=100,
        help_text=_("Short description of the image."))
    image_file = models.ImageField(_("upload a file"),
        upload_to="images/blog/%Y/%m/%d",
        max_length=100,
        blank=True,
        help_text=_("**Does not support SVG or WebP formats.**")
    )
    external_url = models.URLField(_("external URL"),
        help_text=_("For linking to an external image (use with caution)."),
        blank=True
    )

    class Meta:
        ordering = ('-created',)
        verbose_name = _('image')
        verbose_name_plural = _('images')
        get_latest_by = ('-created',)

    def __str__(self):
        if self.pk is not None:
            return "\u007B\u007B {} \u007D\u007D".format(self.pk)
        else:
            return _("deleted image")

    def get_absolute_url(self):
        """
        Returns the URL associated with the :model:`blog.Image` instance.


        If ``image_file`` is used, the path to the uploaded file is returned.

        If ``external_url`` is used, the URL to the image is returned.
        """
        return self.external_url if self.external_url else self.image_file.url

    def clean(self):
        """
        Ensures that the user either uploads an image file or provides an
        external URL, but not both.
        """
        if self.image_file and self.external_url:
            raise ValidationError(_("You may upload a file or use an external URL,"
                                  " but not both."), code='invalid')
        elif not self.image_file and not self.external_url:
            raise ValidationError(_("You are required to either upload an image file or provide"
                                  " an external URL."), code='required')

    def is_external(self):
        """Checks whether the image is hosted externally."""
        return True if self.external_url else False
    is_external.short_description = _("externally hosted")
    is_external.boolean = True